package ua.com.newground.geo_task.async.handlers;

import ua.com.newground.geo_task.async.BaseServiceHelper;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;

public abstract class AbstractCommand implements Runnable{

    public static final int RESULT_SUCCESS = 0;

    public static final int RESULT_FAILURE = -1;
    
    public static final int RESULT_CANCEL = -2;

    public static final String EXTRA_REQUEST_ID = "ua.com.newground.geo_task.async.REQUEST_ID";
    
    public static final String EXTRA_RESULT_RECEIVER = "ua.com.newground.geo_task.async.RESULT_RECEIVER";

	private Context context;

	private ResultReceiver resultReceiver;

	private int requestId;

	private boolean isResultPosted = false;
    
    public AbstractCommand(Context context, Intent intent) {
		this.context = context;
		resultReceiver = intent.getParcelableExtra(EXTRA_RESULT_RECEIVER);
		requestId = intent.getIntExtra(EXTRA_REQUEST_ID, BaseServiceHelper.DEFAULT_REQUEST_ID);
	}
    
    public final void postResult(int resultCode, Bundle resultData){
    	resultReceiver.send(resultCode, resultData);
    	isResultPosted = true;
    }
    
    public boolean isResultPosted() {
		return isResultPosted;
	}
    
    public Context getContext() {
		return context;
	}
    
    public int getRequestId() {
		return requestId;
	}
}
