package ua.com.newground.geo_task.async.service;

import java.lang.reflect.Constructor;
import java.util.HashMap;

import ua.com.newground.geo_task.async.handlers.AbstractCommand;
import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.Process;
import android.text.TextUtils;
import android.util.Log;

public class CommandExecutorService extends IntentService {

	private static final String TAG = "CommandExecutorService";
	
	private static final HashMap<String, Class<? extends AbstractCommand>> COMMANDS_MAP = new HashMap<String, Class<? extends AbstractCommand>>();

	public CommandExecutorService() {
		super(TAG);
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);

		if (TextUtils.isEmpty(intent.getAction())){
			return;
		}
		
		// TODO: implement cancel
		
		AbstractCommand cmd = null;
		try {
			Class<? extends AbstractCommand> cls = COMMANDS_MAP.get(intent.getAction());
			Constructor<? extends AbstractCommand> constructor = cls.getConstructor(Context.class, Intent.class);
			cmd = constructor.newInstance(this, intent);
			cmd.run();
		} catch (Throwable t){
			t.printStackTrace();
		} finally {
			if (cmd != null && !cmd.isResultPosted()){
				cmd.postResult(AbstractCommand.RESULT_FAILURE, null);
				Log.w(TAG, cmd.getRequestId() + " - no any results were posted. RESULT_FAILURE posted manually");
			}
		}
		
		/*
		String action = intent.getAction();
		if (!TextUtils.isEmpty(action)) {
			ResultReceiver receiver = getReceiver(intent);

			if (TestActionHandler.ACTION_EXAMPLE_ACTION.equals(action)) {
				new TestActionHandler().execute(intent,
						getApplicationContext(), receiver);
			}
		}
		*/
	}

	public static void registerCommand(String actionString, Class<? extends AbstractCommand> cls){
		COMMANDS_MAP.put(actionString, cls);
	}
}
