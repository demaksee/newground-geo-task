package ua.com.newground.geo_task.async;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

import ua.com.newground.geo_task.async.handlers.AbstractCommand;
import ua.com.newground.geo_task.async.service.CommandExecutorService;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.util.SparseArray;

public class BaseServiceHelper {

	public static final int DEFAULT_REQUEST_ID = -1;

	private ArrayList<OnServiceResultListener> currentListeners = new ArrayList<OnServiceResultListener>();

	private AtomicInteger idCounter = new AtomicInteger();

	private SparseArray<Intent> pendingActivities = new SparseArray<Intent>();

	private Context context;

	public interface OnServiceResultListener {
	    void onServiceResult(int reqId, Intent reqIntent, int resultCode, Bundle data);
	}
	
	public BaseServiceHelper(Context context) {
		this.context = context;
	}

	public void addListener(OnServiceResultListener currentListener) {
		currentListeners.add(currentListener);
	}

	public void removeListener(OnServiceResultListener currentListener) {
		currentListeners.remove(currentListener);
	}

	public boolean isPending(int requestId) {
		return pendingActivities.get(requestId) != null;
	}

	protected int createId() {
		return idCounter.getAndIncrement();
	}

	protected int runRequest(final int requestId, Intent i) {
		pendingActivities.append(requestId, i);
		context.startService(i);
		return requestId;
	}

	protected Intent createIntent(String actionString,
			final int requestId) {
		Intent i = new Intent(context, CommandExecutorService.class);
		i.setAction(actionString);

		i.putExtra(AbstractCommand.EXTRA_RESULT_RECEIVER,
				new ResultReceiver(new Handler()) {
					@Override
					protected void onReceiveResult(int resultCode,
							Bundle resultData) {
						Intent originalIntent = pendingActivities
								.get(requestId);
						if (isPending(requestId)) {
							pendingActivities.remove(requestId);

							for (OnServiceResultListener currentListener : currentListeners) {
								if (currentListener != null) {
									currentListener.onServiceResult(
											requestId, originalIntent,
											resultCode, resultData);
								}
							}
						}
					}
				});

		return i;
	}

	public void cancelRequest(int requestId){
		if (!isPending(requestId)){
			return;
		}
		Intent originalIntent = pendingActivities
				.get(requestId);
			pendingActivities.remove(requestId);

		for (OnServiceResultListener currentListener : currentListeners) {
			if (currentListener != null) {
				currentListener.onServiceResult(
						requestId, originalIntent,
						AbstractCommand.RESULT_CANCEL, null);
			}
		}
	}
}
