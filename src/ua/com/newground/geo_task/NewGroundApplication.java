package ua.com.newground.geo_task;

import java.util.ArrayList;

import com.google.android.gms.maps.model.LatLng;

import ua.com.newground.geo_task.async.service.CommandExecutorService;
import ua.com.newground.geo_task.command.DirectionCommand;
import ua.com.newground.geo_task.command.GeocodeCommand;
import android.app.Application;
import android.location.Address;

public class NewGroundApplication extends Application {

	private static NewGroundApplication instance;
	
	private ServiceHelper serviceHelper;
	
	private Address addressFrom;
	
	private Address addressTo;
	
	private ArrayList<LatLng> latLngList = new ArrayList<LatLng>();
	
	public void onCreate() {
		instance = this;
		
		// workaround for
		// http://code.google.com/p/android/issues/detail?id=20915
		try {
			Class.forName("android.os.AsyncTask");
		} catch (ClassNotFoundException e) {
		}
		
		serviceHelper = new ServiceHelper(this);
		CommandExecutorService.registerCommand(GeocodeCommand.ACTION, GeocodeCommand.class);
		CommandExecutorService.registerCommand(DirectionCommand.ACTION, DirectionCommand.class);
	};
	
	public ServiceHelper getServiceHelper() {
		return serviceHelper;
	}
			
	public static NewGroundApplication getInstance() {
		return instance;
	}
	
	public Address getAddressFrom() {
		return addressFrom;
	}
	
	public void setAddressFrom(Address addressFrom) {
		this.addressFrom = addressFrom;
	}
	
	public Address getAddressTo() {
		return addressTo;
	}
	
	public void setAddressTo(Address addressTo) {
		this.addressTo = addressTo;
	}
	
	public ArrayList<LatLng> getLatLngList() {
		return latLngList;
	}
	
	public void setLatLngList(ArrayList<LatLng> latLngList) {
		this.latLngList = latLngList;
	}
}
