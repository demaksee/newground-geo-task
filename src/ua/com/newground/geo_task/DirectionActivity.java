package ua.com.newground.geo_task;

import ua.com.newground.geo_task.async.BaseServiceHelper.OnServiceResultListener;
import ua.com.newground.geo_task.async.handlers.AbstractCommand;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.android.gms.maps.model.PolylineOptions;

public class DirectionActivity extends ActionBarActivity implements OnServiceResultListener{

	private NewGroundApplication app;
	private ServiceHelper serviceHelper;
	private SupportMapFragment mapFragment;
	private int requestId;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_direction);
		
		app = NewGroundApplication.getInstance();
		
		mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
		
		serviceHelper = app.getServiceHelper();
		
		requestId = serviceHelper.getDirectionAddress(app.getAddressFrom(), app.getAddressTo());
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		serviceHelper.addListener(this);
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		serviceHelper.removeListener(this);
	}

	@Override
	public void onServiceResult(int reqId, Intent reqIntent, int resultCode,
			Bundle data) {
		if (reqId == requestId){
			if (AbstractCommand.RESULT_SUCCESS == resultCode){
				GoogleMap map = mapFragment.getMap();
//				map.addPolyline(new PolylineOptions().addAll(app.getLatLngList()));
				map.addPolygon(new PolygonOptions().addAll(app.getLatLngList()));
			}
		}
	}
}
