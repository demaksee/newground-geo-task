package ua.com.newground.geo_task.command;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import ua.com.newground.geo_task.async.handlers.AbstractCommand;

public class GeocodeCommand extends AbstractCommand{
	
	public static final String ACTION = "ua.com.newground.geo_task.action.GEOCODE";
	
	public static final String EXTRA_ADDRESS_STRING = "ua.com.newground.geo_task.async.extra.ADDRESS_STRING";
	
	public static final String EXTRA_RESULT_ADDRESS_LIST = "ua.com.newground.geo_task.async.result.ADDRESS_LIST";
	
	private String address;
	
	private Geocoder geocoder;
	
	private static final int MAX_RESULTS = 7;
	
	public GeocodeCommand(Context context, Intent intent) {
		super(context, intent);
		address = intent.getStringExtra(EXTRA_ADDRESS_STRING);
		geocoder = new Geocoder(context);
	}

	@Override
	public void run() {
		
		if (!Geocoder.isPresent()){
			// TODO show message
			return;
		}
		
		try {
			List<Address> list = geocoder.getFromLocationName(address, MAX_RESULTS);
			Bundle b = new Bundle();
			b.putParcelableArrayList(EXTRA_RESULT_ADDRESS_LIST, new ArrayList<Address>(list));
			postResult(RESULT_SUCCESS, b);
			return;
		} catch (IOException e) {
			e.printStackTrace();
		}
		postResult(RESULT_FAILURE, null);
	}

}
