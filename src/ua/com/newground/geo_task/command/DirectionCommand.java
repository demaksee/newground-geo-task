package ua.com.newground.geo_task.command;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import ua.com.newground.geo_task.NewGroundApplication;
import ua.com.newground.geo_task.async.handlers.AbstractCommand;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

public class DirectionCommand extends AbstractCommand{
	
	private static final String TAG = "DirectionCommand";
	
	public static final String ACTION = "ua.com.newground.geo_task.action.DIRECTION";
	
	public static final String EXTRA_ADDRESS_FROM = "ua.com.newground.geo_task.async.extra.ADDRESS_FROM";
	
	public static final String EXTRA_ADDRESS_TO = "ua.com.newground.geo_task.async.extra.ADDRESS_TO";

	private Address from;
	
	private Address to;
	
//	private String url = "https://developers.google.com/maps/documentation/directions/";
	
//	private String url = "http://maps.googleapis.com/maps/api/directions/json?origin=Toronto&destination=Montreal&sensor=false"
	
	public DirectionCommand(Context context, Intent intent) {
		super(context, intent);
		from = intent.getParcelableExtra(EXTRA_ADDRESS_FROM);
		to = intent.getParcelableExtra(EXTRA_ADDRESS_TO);
	}

	@Override
	public void run() {
		
		try {
			HttpGet get = new HttpGet(getUrl());
			HttpClient client = new DefaultHttpClient();
			HttpResponse response = client.execute(get);
			
			int code = response.getStatusLine().getStatusCode();
			Log.d(TAG, "code: " + code);
			if (code != 200){
				postResult(RESULT_FAILURE, null);
				return;
			}
			
			HttpEntity entity = response.getEntity();
			
			JSONObject jsonObject = new JSONObject(convertStreamToString(entity.getContent()));
			String status = jsonObject.getString("status");
			Log.d(TAG, "status: " + status);
			if (!"OK".equals(status)){
				postResult(RESULT_FAILURE, null);
				return;
			}
			
			ArrayList<LatLng> res = new ArrayList<LatLng>();
			
			JSONArray routesJsonArray = jsonObject.getJSONArray("routes");
			JSONArray legsJsonArray = routesJsonArray.getJSONObject(0).getJSONArray("legs");
			for (int i = 0; i < legsJsonArray.length(); i++){
				JSONObject legObj = legsJsonArray.getJSONObject(i);
				JSONArray stepJsonArray = legObj.getJSONArray("steps");
				for (int j = 0; j < stepJsonArray.length(); j++){
					JSONObject stepObj = stepJsonArray.getJSONObject(j);
					JSONObject start = stepObj.getJSONObject("start_location");
					LatLng latLng = new LatLng(start.getDouble("lat"), start.getDouble("lng"));
					Log.d(TAG, latLng.toString());
					res.add(latLng);
					if (j == legsJsonArray.length()-1){
						JSONObject end = stepObj.getJSONObject("end_location");
						LatLng latLngEnd = new LatLng(end.getDouble("lat"), end.getDouble("lng"));
						Log.d(TAG, latLngEnd.toString());
						res.add(latLngEnd);
					}
				}
			}
			
			NewGroundApplication.getInstance().setLatLngList(res);
						
			postResult(RESULT_SUCCESS, null);
			return;
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		postResult(RESULT_FAILURE, null);
	}
	
	private String getUrl(){
		String url = "http://maps.googleapis.com/maps/api/directions/json?origin=";
		url += from.getLocality();
		url += "&destination=";
		url += to.getLocality();
		url += "&sensor=false";
		return url;
	}
	
	/*
	 * http://stackoverflow.com/questions/309424/read-convert-an-inputstream-to-a-string
	 */
	static String convertStreamToString(java.io.InputStream is) {
	    java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
	    return s.hasNext() ? s.next() : "";
	}
}
