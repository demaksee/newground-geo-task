package ua.com.newground.geo_task;

import ua.com.newground.geo_task.async.BaseServiceHelper;
import ua.com.newground.geo_task.command.DirectionCommand;
import ua.com.newground.geo_task.command.GeocodeCommand;
import android.content.Context;
import android.content.Intent;
import android.location.Address;

public class ServiceHelper extends BaseServiceHelper{

	public ServiceHelper(Context context) {
		super(context);
	}
	
	public int geocodeAddress(String address) {
		final int requestId = createId();

		Intent i = createIntent(GeocodeCommand.ACTION, requestId);
		i.putExtra(GeocodeCommand.EXTRA_ADDRESS_STRING, address);

		return runRequest(requestId, i);
	}
	
	public int getDirectionAddress(Address from, Address to) {
		final int requestId = createId();

		Intent i = createIntent(DirectionCommand.ACTION, requestId);
		i.putExtra(DirectionCommand.EXTRA_ADDRESS_FROM, from);
		i.putExtra(DirectionCommand.EXTRA_ADDRESS_TO, to);

		return runRequest(requestId, i);
	}

}
