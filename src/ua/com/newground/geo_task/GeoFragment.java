package ua.com.newground.geo_task;

import java.util.ArrayList;
import java.util.List;

import ua.com.newground.geo_task.async.BaseServiceHelper;
import ua.com.newground.geo_task.async.BaseServiceHelper.OnServiceResultListener;
import ua.com.newground.geo_task.async.handlers.AbstractCommand;
import ua.com.newground.geo_task.command.GeocodeCommand;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.internal.widget.ListPopupWindow;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.MarkerOptionsCreator;

public class GeoFragment extends Fragment implements OnServiceResultListener,
		OnItemClickListener {

	protected static final String TAG = "GeoFragment";

	private static final String POS_TAG = "pos";

	private int pos;

	private EditText searchEditText;

	private ServiceHelper serviceHelper;

	private ListPopupWindow listPopupWindow;

	private int requestId;

	private AddressAdapter adapter;

	private Address address;

	private SupportMapFragment mapFragment;

	private NewGroundApplication app;

	public static GeoFragment newInstance(int pos) {
		GeoFragment fragment = new GeoFragment();
		Bundle b = new Bundle();
		b.putInt(POS_TAG, pos);
		fragment.setArguments(b);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		pos = getArguments().getInt(POS_TAG);
		app = NewGroundApplication.getInstance();
		serviceHelper = app.getServiceHelper();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View v = inflater.inflate(R.layout.fragment_geo, container, false);

		mapFragment = new SupportMapFragment();
		FragmentManager fragmentManager = getChildFragmentManager();
		fragmentManager.beginTransaction()
				.add(R.id.map_container, mapFragment, String.valueOf(pos))
				.commit();

		searchEditText = (EditText) v.findViewById(R.id.editTextSearch);

		return v;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		listPopupWindow = new ListPopupWindow(getActivity());
		// listPopupWindow.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
		// listPopupWindow.setPromptPosition(ListPopupWindow.POSITION_PROMPT_BELOW);
		listPopupWindow.setHeight(getResources().getDimensionPixelSize(
				R.dimen.list_item_height) * 3);
		listPopupWindow.setBackgroundDrawable(new ColorDrawable(0xffffffff));
		listPopupWindow.setAnchorView(searchEditText);
		adapter = new AddressAdapter(getActivity(), new ArrayList<Address>());
		listPopupWindow.setAdapter(adapter);

		listPopupWindow.setOnItemClickListener(this);

		searchEditText.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				if (requestId != BaseServiceHelper.DEFAULT_REQUEST_ID) {
					serviceHelper.cancelRequest(requestId);
				}
				requestId = serviceHelper.geocodeAddress(searchEditText
						.getText().toString());
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {
			}
		});
	}

	@Override
	public void onResume() {
		super.onResume();
		serviceHelper.addListener(this);
	}

	@Override
	public void onPause() {
		super.onPause();
		serviceHelper.removeListener(this);
	}

	@Override
	public void onServiceResult(int reqId, Intent reqIntent, int resultCode,
			Bundle data) {
		if (requestId == reqId) {
			requestId = BaseServiceHelper.DEFAULT_REQUEST_ID;
			if (resultCode == AbstractCommand.RESULT_SUCCESS) {

				List<Address> list = data
						.getParcelableArrayList(GeocodeCommand.EXTRA_RESULT_ADDRESS_LIST);
				adapter.setAddressList(list);
				// listPopupWindow.setDropDownAlwaysVisible(true);
				if (!listPopupWindow.isShowing()) {
					listPopupWindow.show();
				}

				// for (Address address : list){
				// Log.d(TAG, address.toString());
				// }
			}
		}
	}

	private static class AddressAdapter extends BaseAdapter {

		private List<Address> list;
		private LayoutInflater inflater;

		public AddressAdapter(Context context, List<Address> list) {
			inflater = LayoutInflater.from(context);
			this.list = list;

		}

		public void setAddressList(List<Address> list) {
			this.list = list;
			notifyDataSetInvalidated();
		}

		@Override
		public int getCount() {
			return list.size();
		}

		@Override
		public Object getItem(int position) {
			return list.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view = convertView;
			if (convertView == null) {
				view = inflater.inflate(R.layout.list_item, parent, false);
			}

			Address address = list.get(position);

			TextView tv = (TextView) view;

			tv.setText(address.getFeatureName() + " " + address.getCountryName());

			return view;
		}

		@Override
		public View getDropDownView(int position, View convertView,
				ViewGroup parent) {
			View view = convertView;
			if (convertView == null) {
				view = inflater.inflate(R.layout.list_item, parent, false);
			}

			Address address = list.get(position);

			TextView tv = (TextView) view;

			tv.setText(address.getFeatureName() + " " + address.getCountryName());
			
			return view;
		}
	}

	@Override
	public void onItemClick(AdapterView<?> adapterView, View view,
			int position, long id) {
		listPopupWindow.dismiss();
		address = (Address) adapter.getItem(position);
		searchEditText.setText(address.getFeatureName());
		
		if (pos == 0){
			app.setAddressFrom(address);
		} else {
			app.setAddressTo(address);
		}

		showAddressOnMap();
	}

	private void showAddressOnMap() {
		GoogleMap map = mapFragment.getMap();
		map.clear();
		LatLng position = new LatLng(address.getLatitude(), address.getLongitude());
		map.addMarker(new MarkerOptions().position(
				position)
				.title(address.getFeatureName()));
		
		map.animateCamera(CameraUpdateFactory.newLatLng(position));
	}
}
